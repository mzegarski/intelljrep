package pl.pawel.zoo.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.pawel.zoo.entity.Animal;
import pl.pawel.zoo.util.HibernateUtil;

import java.util.List;

/**
 * Created by pablojev on 12.07.2017.
 */
public class AnimalDAO implements AbstractDAO<Animal> {
    public boolean insert(Animal type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.save(type);
        t.commit();
        session.close();
        return true;
    }

    public boolean delete(Animal type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(type);
        t.commit();
        if(this.get(type.getId()) == null) {
            return true;
        }
        session.close();
        return false;
    }

    public boolean delete(int id) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(this.get(id));
        t.commit();
        session.close();
        return true;
    }

    public boolean update(Animal type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.update(type);
        t.commit();
        session.close();
        return true;
    }

    public Animal get(int id) {
        Animal animal;
        Session session = HibernateUtil.openSession();
        animal = session.load(Animal.class, id);
        session.close();
        return animal;
    }

    public List<Animal> get() {
        List<Animal> animals;
        Session session = HibernateUtil.openSession();
        animals = session.createQuery("from Animal").list();
        session.close();
        return animals;
    }
}
