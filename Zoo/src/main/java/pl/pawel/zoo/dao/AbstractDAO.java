package pl.pawel.zoo.dao;

import java.util.List;

/**
 * Created by pablojev on 12.07.2017.
 */
public interface AbstractDAO<T> {
    boolean insert(T type);
    boolean delete(T type);
    boolean delete(int id);
    boolean update(T type);
    T get(int id);
    List<T> get();
}
