import personDecorator.*;
import sportsmanDecorator.*;

/**
 * Created by zegarski on 2017-06-23.
 */
public class Main {

    public static void main(String[] args) {

        //sportsmanDecorator();

        //personDecorator();






    }

    private static void personDecorator() {
        Person person =
                new PersonBuilder()
                        .withName("Janek")
                        .withSurname("Kowalski")
                        .withAge(21)
                        .withWeight(85.5)
                        .withHeight(180)
                        .withEyesColor(Person.EyesColor.BROWN)
                        .createPerson();

        PersonPrinter ps = new AgePrinter(new BasicDataPrinter());

        ConsolPrinter cs = new ConsolPrinter(ps);

        cs.printToConsol(person);

        FilePrinter fp = new FilePrinter(ps);

        fp.printToFile(person, "test.txt");
    }

    private static void sportsmanDecorator() {
        Sportsman sportsman = new NoPrepare(new DoubleSeries(new SelfieMaking(new WaterDrinking(new BasicSportsman()))));
        Sportsman sportsman1 = (new DoubleSeries(new WaterDrinking(new BasicSportsman())));
        FitnessStudio fs = new FitnessStudio();
        fs.train(sportsman1);
    }

}
