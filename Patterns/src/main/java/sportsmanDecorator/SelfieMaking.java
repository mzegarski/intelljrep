package sportsmanDecorator;

/**
 * Created by zegarski on 2017-06-23.
 */
public class SelfieMaking implements Sportsman {

    private Sportsman sportsman;

    public SelfieMaking(Sportsman sportsman) {
        this.sportsman = sportsman;
    }
    @Override
    public void prepare() {
        sportsman.prepare();
        selfie();
    }



    @Override
    public void doPumps(int pumps) {

        sportsman.doPumps(pumps);
        selfie();

    }

    @Override
    public void doSquats(int squats) {
        sportsman.doSquats(squats);
        selfie();

    }

    @Override
    public void doCrunches(int crunches) {
        sportsman.doCrunches(crunches);
        selfie();

    }

    private void selfie() {
        System.out.println("Time to take a selfie!");
    }

}
