package sportsmanDecorator;

/**
 * Created by zegarski on 2017-06-23.
 */
public class FitnessStudio {


    public void train(Sportsman sportsman){
        sportsman.prepare();
        sportsman.doPumps(1);
        sportsman.doSquats(2);
        sportsman.doCrunches(5);
    }



}
