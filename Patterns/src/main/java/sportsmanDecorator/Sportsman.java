package sportsmanDecorator;

/**
 * Created by zegarski on 2017-06-23.
 */
public interface Sportsman {

    void prepare();

    void doPumps(int pumps);

    void doSquats (int squats);

    void doCrunches(int crunches);


}



