package sportsmanDecorator;

/**
 * Created by zegarski on 2017-06-23.
 */
public class BasicSportsman implements Sportsman {


    @Override
    public void prepare() {
        System.out.println("Rozgrzewka...");
    }

    @Override
    public void doPumps(int pumps) {

        for (int i = 0; i < pumps; i++)
            System.out.println("Robie pompke nr " + (i + 1));

    }

    @Override
    public void doSquats(int squats) {

        for (int i = 0; i < squats; i++)
            System.out.println("Robie przysiad nr " + (i + 1));

    }

    @Override
    public void doCrunches(int crunches) {

        for (int i = 0; i < crunches; i++)
            System.out.println("Robie brzuszek nr " + (i + 1));

    }
}
