package sportsmanDecorator;

/**
 * Created by zegarski on 2017-06-23.
 */
public class WaterDrinking implements Sportsman {

    private Sportsman sportsman;

    public WaterDrinking(Sportsman sportsman) {
        this.sportsman = sportsman;
    }

    @Override
    public void prepare() {

        sportsman.prepare();
        drinkingWater();

    }



    @Override
    public void doPumps(int pumps) {

        sportsman.doPumps(pumps);
        drinkingWater();

    }

    @Override
    public void doSquats(int squats) {

        sportsman.doSquats(squats);
        drinkingWater();

    }

    @Override
    public void doCrunches(int crunches) {

        sportsman.doCrunches(crunches);
        drinkingWater();

    }

    private void drinkingWater() {
        System.out.println("Pije wode.");
    }
}
