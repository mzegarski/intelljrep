package sportsmanDecorator;

/**
 * Created by zegarski on 2017-06-23.
 */
public class NoPrepare implements Sportsman {

    private Sportsman sportsman;

    public NoPrepare(Sportsman sportsman) {
        this.sportsman = sportsman;
    }

    @Override
    public void prepare() {


    }

    @Override
    public void doPumps(int pumps) {

        sportsman.doPumps(pumps);

    }

    @Override
    public void doSquats(int squats) {

        sportsman.doSquats(squats);

    }

    @Override
    public void doCrunches(int crunches) {

        sportsman.doCrunches(crunches);

    }
}
