package personDecorator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

/**
 * Created by zegarski on 2017-06-23.
 */
public class FilePrinter {

    private final PersonPrinter personPrinter;

    public FilePrinter(PersonPrinter personPrinter) {
        this.personPrinter = personPrinter;
    }

    public void printToFile(Person person, String filename) {

        File file = new File("src/main/resources/" + filename);

        try (PrintStream ps = new PrintStream(file)) {

            personPrinter.Print(person, ps);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }
}
