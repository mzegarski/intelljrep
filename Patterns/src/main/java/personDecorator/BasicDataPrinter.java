package personDecorator;

import java.io.PrintStream;

/**
 * Created by zegarski on 2017-06-23.
 */
public class BasicDataPrinter implements PersonPrinter {


    @Override
    public void Print(Person person, PrintStream out) {
        out.println("name : " + person.getName());
        out.println("surname : " + person.getSurname());
    }
}

