package personDecorator;

/**
 * Created by zegarski on 2017-06-23.
 */
public class ConsolPrinter {

    private final PersonPrinter personPrinter;

    public ConsolPrinter(PersonPrinter personPrinter) {
        this.personPrinter = personPrinter;
    }

    public void printToConsol(Person person) {
        personPrinter.Print(person, System.out);
    }
}
