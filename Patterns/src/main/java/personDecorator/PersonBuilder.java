package personDecorator;

public class PersonBuilder {
    private String name;
    private String surname;
    private int age;
    private double weight;
    private double height;
    private Person.EyesColor eyesColor;

    public PersonBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public PersonBuilder withSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public PersonBuilder withAge(int age) {
        this.age = age;
        return this;
    }

    public PersonBuilder withWeight(double weight) {
        this.weight = weight;
        return this;
    }

    public PersonBuilder withHeight(double height) {
        this.height = height;
        return this;
    }

    public PersonBuilder withEyesColor(Person.EyesColor eyesColor) {
        this.eyesColor = eyesColor;
        return this;
    }

    public Person createPerson() {
        return new Person(name, surname, age, weight, height, eyesColor);
    }
}