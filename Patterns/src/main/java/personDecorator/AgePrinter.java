package personDecorator;

import java.io.PrintStream;

/**
 * Created by zegarski on 2017-06-23.
 */
public class AgePrinter implements PersonPrinter {

    private final PersonPrinter personPrinter;

    public AgePrinter(PersonPrinter personPrinter) {
        this.personPrinter = personPrinter;
    }


    @Override
    public void Print(Person person, PrintStream out) {
        personPrinter.Print(person, out);
        out.println("age : " + person.getAge());

    }
}
