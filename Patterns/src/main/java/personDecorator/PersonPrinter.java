package personDecorator;

import java.io.PrintStream;

/**
 * Created by zegarski on 2017-06-23.
 */
public interface PersonPrinter {

    void Print(Person person, PrintStream printStream);


}
