package personDecorator;

import java.io.PrintStream;

/**
 * Created by zegarski on 2017-06-23.
 */
public class WeightPrinter implements PersonPrinter {

    private final PersonPrinter personPrinter;

    public WeightPrinter(PersonPrinter personPrinter) {
        this.personPrinter = personPrinter;
    }


    @Override
    public void Print(Person person, PrintStream out) {
        personPrinter.Print(person, out);
        out.println("Weight: " + person.getWeight());
    }
}
