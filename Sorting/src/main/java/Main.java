public class Main {

    private static int[] tab = {9, 0, 2, 3, 1, 8, 7, 6, 5, 4};

    private static int N = tab.length;

    private static int t[] = new int[N];

    public static void main(String[] args) {


        //mySorting(tab);

        BubbleSorting(tab);

        //insertSort(tab);

        // selectionSort(tab);

        // mergeSorting(0, N - 1);


        System.out.println("Finish: ");
        for (int t : tab) System.out.print(t + " ");


    }

    private static void selectionSort(int[] tab) {
        for (int i = 0; i < tab.length; i++) {
            int minIndex = i;

            for (int j = i; j < tab.length; j++) {
                if (tab[minIndex] > tab[j])
                    minIndex = j;
            }
            int bufor = tab[i];
            tab[i] = tab[minIndex];
            tab[minIndex] = bufor;

        }
    }

    private static void insertSort(int[] tab) {
        for (int i = 1; i < tab.length; i++) {
            int position = i;
            int currentPosition = tab[i];

            while (position > 0 && tab[position - 1] > currentPosition) {
                tab[position] = tab[position - 1];
                position--;
            }
            tab[position] = currentPosition;


        }
    }

    private static void mySorting(int[] tab) {
        boolean isTabSorted;
        int temp;

        do {
            isTabSorted = true;
            for (int i = 0; i < tab.length - 1; i++) {
                if (tab[i] > tab[i + 1]) {
                    temp = tab[i];
                    tab[i] = tab[i + 1];
                    tab[i + 1] = temp;
                    isTabSorted = false;
                }
            }

        } while (!isTabSorted);
    }

    private static void BubbleSorting(int[] tab) {
        boolean isTabSorted;
        int temp;
        int sizeReduction = 1;

        do {
            isTabSorted = true;
            for (int i = 0; i < tab.length - sizeReduction; i++) {
                if (tab[i] > tab[i + 1]) {
                    temp = tab[i];
                    tab[i] = tab[i + 1];
                    tab[i + 1] = temp;
                    isTabSorted = false;
                }
            }
            sizeReduction++;

        } while (!isTabSorted);
    }

    public static void mergeSorting(int start, int end) {
        int middle;

        if (start < end) {
            middle = (start + end) / 2;
            mergeSorting(start, middle);
            mergeSorting(middle + 1, end);
            merge(start, middle, end);
        }
    }

    private static void merge(int start, int middle, int end) {
        int i, j, q;
        for (i = start; i <= end; i++) t[i] = tab[i];
        i = start;
        j = middle + 1;
        q = start;
        while (i <= middle && j <= end) {
            if (t[i] < t[j])
                tab[q++] = t[i++];
            else
                tab[q++] = t[j++];
        }
        while (i <= middle)
            tab[q++] = t[i++];
    }


}
