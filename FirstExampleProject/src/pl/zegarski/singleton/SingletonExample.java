package pl.zegarski.singleton;


public class SingletonExample {

    private static SingletonExample _instance;

    private SingletonExample() {}

    public static SingletonExample getInstance() {
        //lazy initialization

        if (_instance == null) {
            System.out.println("Tworze instancje.");
            _instance = new SingletonExample();
        }
        System.out.println("Zwracam instancje, bo jest juz utworzona");
        return _instance;
    }


}
