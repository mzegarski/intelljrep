package pl.zegarski.carFactory;

/**
 * Created by zegarski on 2017-06-22.
 */
public class CarFactory {

    public static CarInterface getCar(String carName) {

        carName = carName.toLowerCase();

        switch (carName) {
            case "maluch":
                return new Maluch();
            case "BMW":
                return new BMW();
            case "Porsche":
                return new Porsche();
        }
        throw new IllegalArgumentException("That car does not exist");


    }
}
