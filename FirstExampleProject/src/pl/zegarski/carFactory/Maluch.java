package pl.zegarski.carFactory;

/**
 * Created by zegarski on 2017-06-22.
 */
public class Maluch implements CarInterface {
    @Override
    public void getMaxSpeed() {
        System.out.println("60 km/h");
    }
}
