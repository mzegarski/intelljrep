package pl.zegarski.carFactory;

/**
 * Created by zegarski on 2017-06-22.
 */
public class Porsche implements CarInterface {
    @Override
    public void getMaxSpeed() {
        System.out.println("220 km/h");
    }
}
