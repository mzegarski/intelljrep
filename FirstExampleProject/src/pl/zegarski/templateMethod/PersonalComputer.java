package pl.zegarski.templateMethod;

/**
 * Created by zegarski on 2017-06-22.
 */
public class PersonalComputer  extends BasicComputer{
    @Override
    public void externalDevice() {
        System.out.println("Mouse & Keyboard");
    }
}
