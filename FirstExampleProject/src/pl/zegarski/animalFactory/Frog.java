package pl.zegarski.animalFactory;

/**
 * Created by zegarski on 2017-06-22.
 */
public class Frog implements AnimalInterface{
    @Override
    public void getSound() {
        System.out.println("kum kum");
    }
}
