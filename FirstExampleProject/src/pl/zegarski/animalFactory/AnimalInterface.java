package pl.zegarski.animalFactory;

/**
 * Created by zegarski on 2017-06-22.
 */
public interface AnimalInterface {

    public void getSound();

}
