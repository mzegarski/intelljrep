package pl.zegarski.animalFactory;

/**
 * Created by zegarski on 2017-06-22.
 */
public class Cat implements AnimalInterface {
    @Override
    public void getSound() {
        System.out.println("moew moew");
    }
}
