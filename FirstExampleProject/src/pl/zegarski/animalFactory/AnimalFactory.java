package pl.zegarski.animalFactory;

/**
 * Created by zegarski on 2017-06-22.
 */
public class AnimalFactory {

    public static AnimalInterface getAnimal(String animal) {

        animal = animal.toLowerCase();
        switch (animal) {
            case "dog":
                return new Dog();
            case "frog":
                return new Frog();
            case "cat":
                return new Cat();
        }
        throw new IllegalArgumentException("Unknow animal.");
    }
}
