package pl.zegarski.FluidInterface;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by zegarski on 2017-06-22.
 */
public class Beer {

    private HashMap<String, List<Beer>> beerList = new HashMap<>();
    private List<Beer> currentSearch;

    private String name;
    private String taste;
    private String type;
    private double price;

    public Beer addGroup(String groupName, List<Beer> beerz) {
        beerList.put(groupName, beerz);
        return this;
    }

    public Beer from(String from) {

        currentSearch = beerList.get(from);
        return this;
    }

    public Beer name(String name) {
        List<Beer> newList = new LinkedList<>();
        for (Beer b : currentSearch) {
            if (b.getName().equals(name)) {
                newList.add(b);
            }

        }
        return this;
    }

    public Beer taste(String taste) {
        List<Beer> newList = new LinkedList<>();
        for (Beer b : currentSearch) {
            if (b.getTaste().equals(taste)) {
                newList.add(b);
            }

        }

        return this;
    }

    public List<Beer> get() {
//         List<Beer> ret = currentSearch;

        return currentSearch;
    }

//    TODO tutaj coś nie sortuje poprawnie, do ogarnięcia

    pubic Beer() {
    }

    public String getName() {
        return name;
    }

    public Beer setName(String name) {
        this.name = name;
        return this;
    }

    public String getTaste() {
        return taste;
    }

    public Beer setTaste(String taste) {
        this.taste = taste;
        return this;
    }

    public String getType() {
        return type;
    }

    public Beer setType(String type) {
        this.type = type;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public Beer setPrice(double price) {
        this.price = price;
        return this;
    }

    @Override
    public String toString() {
        return "Beer{" +
                "name='" + name + '\'' +
                ", taste='" + taste + '\'' +
                ", type='" + type + '\'' +
                ", price=" + price +
                '}';
    }
}
