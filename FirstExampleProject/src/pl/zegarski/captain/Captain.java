package pl.zegarski.captain;

/**
 * Created by zegarski on 2017-06-22.
 */
public class Captain {

    private static Captain _instance;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    private Captain() {
    }

    public static Captain get_instance() {

        if (_instance == null) {
            System.out.println("Tworze kapitana");
            _instance = new Captain();
        }

        System.out.println("Kapitan już utworzony");
        return _instance;
    }


}
