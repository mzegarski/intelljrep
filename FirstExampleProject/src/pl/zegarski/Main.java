package pl.zegarski;

import pl.zegarski.FluidInterface.Beer;
import pl.zegarski.animalFactory.AnimalFactory;
import pl.zegarski.animalFactory.AnimalInterface;
import pl.zegarski.captain.Captain;
import pl.zegarski.carFactory.CarFactory;
import pl.zegarski.carFactory.CarInterface;
import pl.zegarski.numbersFactory.Coupon;
import pl.zegarski.numbersFactory.CouponFactory;
import pl.zegarski.politechnika.Budownictwo;
import pl.zegarski.politechnika.ETI;
import pl.zegarski.politechnika.Okretownictwo;
import pl.zegarski.templateMethod.Laptop;
import pl.zegarski.templateMethod.MidiTowerComputer;
import pl.zegarski.templateMethod.PersonalComputer;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

//        Singleton Pattern
//        capitan();

//        Template Method Pattern
//        computers();
//        politechnika();

//        Factory Pattern
//        AnimalFactory();
//        NumberFactory();
//        CarFactory();


//        Chaining
//        Beer piwko = new Beer().setName("EB").setPrice(2.5).setTaste("sweet").setType("whatever");
//
//        System.out.println(piwko);


//        FluidInterface

        List<Beer> beers = new LinkedList<>();

        beers.add(new Beer().setName("EB").setPrice(2.2).setTaste("sweet").setType("whatever"));
        beers.add(new Beer().setName("EB").setPrice(2.7).setTaste("bitter").setType("whatever"));
        beers.add(new Beer().setName("Zywiec").setPrice(2.9).setTaste("sweet").setType("whatever"));
        beers.add(new Beer().setName("Warka").setPrice(2.1).setTaste("bitter").setType("whatever"));
        beers.add(new Beer().setName("Specjal").setPrice(2.0).setTaste("sweet").setType("whatever"));

        Beer bb = new Beer().addGroup("sikacze", beers);

        for (Beer b : bb.from("sikacze").name("Zywiec").get()) {
            System.out.println(b);

        }



    }

    private static void CarFactory() {
        CarInterface maluch = CarFactory.getCar("Maluch");
        System.out.println("Maluch: ");
        maluch.getMaxSpeed();
    }

    private static void NumberFactory() {
        int[] numbers = new int[6];
        for (int i = 0; i < numbers.length; i++)
            numbers[i] = new Random().nextInt(48) + 1;

        Coupon c = CouponFactory.getCoupon(numbers);
        System.out.println(c.getA());
    }

    private static void AnimalFactory() {
        AnimalInterface cat = AnimalFactory.getAnimal("cat");
        cat.getSound();

        AnimalInterface dog = AnimalFactory.getAnimal("DOG");
        dog.getSound();

        AnimalInterface frog = AnimalFactory.getAnimal("fROG");
        frog.getSound();

        AnimalInterface turtle = AnimalFactory.getAnimal("TuRtLe");
        turtle.getSound();
    }

    private static void politechnika() {
        ETI eti = new ETI();
        System.out.println("ETI: ");
        eti.przedmioty();

        Budownictwo bud = new Budownictwo();
        System.out.println("Budownictwo");
        bud.przedmioty();

        Okretownictwo okr = new Okretownictwo();
        System.out.println("Okrętownictwo");
        okr.przedmioty();
    }

    private static void computers() {
        Laptop laptop = new Laptop();
        System.out.println("Laptop: ");
        laptop.devices();

        MidiTowerComputer mtc = new MidiTowerComputer();
        System.out.println("Midi Tower Computer: ");
        mtc.devices();

        PersonalComputer pc = new PersonalComputer();
        System.out.println("PC: ");
        pc.devices();

    }

    private static void capitan() {
        Captain cap1 = Captain.get_instance();
        Captain cap2 = Captain.get_instance();

        cap1.setName("Maciek");

        System.out.println(cap2.getName());
    }
}
