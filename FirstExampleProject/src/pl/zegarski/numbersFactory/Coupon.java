package pl.zegarski.numbersFactory;

/**
 * Created by zegarski on 2017-06-22.
 */
public class Coupon {

    private int a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;

    public Coupon(int a, int b, int c, int d, int e, int f) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }

    public int getD() {
        return d;
    }

    public int getE() {
        return e;
    }

    public int getF() {
        return f;
    }








}
