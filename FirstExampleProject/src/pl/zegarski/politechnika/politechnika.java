package pl.zegarski.politechnika;

/**
 * Created by zegarski on 2017-06-22.
 */
public abstract class politechnika {

    public void przedmioty(){
        math();
        physic();
        wdw();
        specialization();
        System.out.println();

    }

    public void math(){
        System.out.println("Matematyka");
    }

    public void physic(){
        System.out.println("Fizyka");
    }

    public void wdw(){
        System.out.println("Wyklad do wyboru");
    }

    public abstract void specialization();



}
