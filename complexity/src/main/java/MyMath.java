import java.util.Random;

/**
 * Created by zegarski on 2017-06-26.
 */
public class MyMath {


    public static int sum(int a, int b) { // O(1)

        return a + b;
    }

    public static int pow(int a, int b) { // O(b)

        int result = 1;
        for (int i = 0; i < b; i++)
            result *= a;

        return result;
    }

    public static int[] randArray(int size) {

        int[] tab = new int[size];

        Random r = new Random();

        for (int i = 0; i < size; i++)
            tab[i] = r.nextInt(100);


        return tab;
    }




}
