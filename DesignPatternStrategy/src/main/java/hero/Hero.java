package hero;

/**
 * Created by zegarski on 2017-06-29.
 */
public class Hero {

    private String Name;
    private int hp;
    private int mana;
    private IStrategy strategy = new StrategyKnight();

    public Hero(String name, int hp, int mana) {
        Name = name;
        this.hp = hp;
        this.mana = mana;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public IStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(IStrategy strategy) {
        this.strategy = strategy;
    }

    public void fightDragon(){

      strategy.fight();


    }

}
