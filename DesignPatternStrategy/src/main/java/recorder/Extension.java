package recorder;

/**
 * Created by zegarski on 2017-06-29.
 */
public enum Extension {

    MP4("mp4"), MP3("mp3"), MKV("mkv"), AVI("avi");

    String type;

    Extension(String type) {
        this.type = type.toLowerCase();
    }
}
