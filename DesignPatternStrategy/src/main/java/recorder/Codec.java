package recorder;

/**
 * Created by zegarski on 2017-06-29.
 */
public enum Codec {

    H264("h264"), H263("h263"), THEORA("theora"), VR8("vr8");

    String type;

    Codec(String type) {
        this.type = type.toLowerCase();
    }
}
