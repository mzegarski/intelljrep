package recorder;

/**
 * Created by zegarski on 2017-06-29.
 */
public class Profile {


    private Codec currentCodec;
    private Extension currentExtension;
    private Resolution currentResolution;

    public Profile(Codec currentCodec, Extension currentExtension, Resolution currentResolution) {
        this.currentCodec = currentCodec;
        this.currentExtension = currentExtension;
        this.currentResolution = currentResolution;
    }

    public Codec getCurrentCodec() {
        return currentCodec;
    }

    public void setCurrentCodec(Codec currentCodec) {
        this.currentCodec = currentCodec;
    }

    public Extension getCurrentExtension() {
        return currentExtension;
    }

    public void setCurrentExtension(Extension currentExtension) {
        this.currentExtension = currentExtension;
    }

    public Resolution getCurrentResolution() {
        return currentResolution;
    }

    public void setCurrentResolution(Resolution currentResolution) {
        this.currentResolution = currentResolution;
    }
}
