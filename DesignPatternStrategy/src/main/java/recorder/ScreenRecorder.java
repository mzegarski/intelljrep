package recorder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zegarski on 2017-06-29.
 */
public class ScreenRecorder {

    private List<Profile> profileList = new ArrayList<>();
    private Profile selectedProfile;
    private boolean isRecording = false;


    public void addProfile(Profile newProfile) {

        profileList.add(newProfile);

    }

    public void setProfile(int profileIndex) {


        if (profileIndex > profileList.size() || profileIndex < 0)

            System.out.println(profileIndex + " znajduje sie poza zakresem listy profili " +
                    "[" + profileList.size() + "].");
        else {
            selectedProfile = profileList.get(profileIndex);
        }
    }

    public void listProfile() {
        System.out.println("Lista profili: ");
        for (Profile p : profileList) {
            System.out.print(
                    "Index: " + profileList.indexOf(p)
                            + " = Codec: " + p.getCurrentCodec()
                            + "; Extension: " + p.getCurrentExtension()
                            + "; Resolution: " + p.getCurrentResolution());
            System.out.println();
        }
    }

    public void startRecording() {

        if (!isRecording) {
            if (selectedProfile != null) {
                isRecording = true;
                System.out.println("Wybrany profil: ");
                System.out.print(
                        "Codec: " + selectedProfile.getCurrentCodec()
                                + "; Extension: " + selectedProfile.getCurrentExtension()
                                + "; Resolution: " + selectedProfile.getCurrentResolution() + "\n");
                System.out.println("Rozpoczynam nagrywanie.");
            } else {
                System.out.println("Nie wybrano profilu.");
            }
        } else {
            System.out.println("Juz nagrywam...");
        }
    }

    public void stopRecording() {
        if (isRecording) {
            isRecording = false;
            System.out.println("Koncze nagrywanie.");
        } else {
            System.out.println("Obecnie nie nagrywam...");
        }
    }
}
