package recorder;

import static recorder.Codec.*;
import static recorder.Extension.*;
import static recorder.Resolution.*;

/**
 * Created by zegarski on 2017-06-29.
 */
public class Main {

    public static void main(String[] args) {

        ScreenRecorder recorder = new ScreenRecorder();

        recorder.addProfile(new Profile(H263, AVI, HIGHRES));
        recorder.addProfile(new Profile(H264, MKV, LOWRES));
        recorder.addProfile(new Profile(VR8, MP3, MEDIUMRES));
        recorder.listProfile();
        recorder.setProfile(2);

        recorder.startRecording();
        recorder.startRecording();
        recorder.stopRecording();
        recorder.stopRecording();





    }
}
