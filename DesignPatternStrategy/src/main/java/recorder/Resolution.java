package recorder;

/**
 * Created by zegarski on 2017-06-29.
 */
public enum Resolution {

    HIGHRES("highres"), MEDIUMRES("mediumres"), LOWRES("lowres");

    String type;

    Resolution(String type) {
        this.type = type.toLowerCase();
    }
}
