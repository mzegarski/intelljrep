package cityhall;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zegarski on 2017-06-29.
 */
public class Departament {

    List<Office> departamentList = new ArrayList<>();

    public void addOffice() {

        departamentList.add(new Office());
    }

    public Office getOffice(int index) {

        return departamentList.get(index);
    }

}
