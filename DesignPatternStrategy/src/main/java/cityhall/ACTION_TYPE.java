package cityhall;

/**
 * Created by zegarski on 2017-06-29.
 */
public enum ACTION_TYPE {
    REGISTER, UNREGISTER, CHECK_STATUS
}
