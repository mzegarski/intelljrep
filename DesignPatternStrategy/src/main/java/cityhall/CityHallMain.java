package cityhall;

/**
 * Created by zegarski on 2017-06-29.
 */
public class CityHallMain {

    public static void main(String[] args) {

        Departament cityHall = new Departament();

        cityHall.addOffice();
        cityHall.addOffice();

        Client c1 = new Client(ACTION_TYPE.REGISTER, 871117);

        cityHall.getOffice(1).handle(c1);

    }
}
