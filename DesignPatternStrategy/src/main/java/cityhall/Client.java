package cityhall;

/**
 * Created by zegarski on 2017-06-29.
 */
public class Client {

    ACTION_TYPE action;
    long pesel;

    public Client(ACTION_TYPE action, int pesel) {
        this.action = action;
        this.pesel = pesel;
    }

    public ACTION_TYPE getAction() {
        return action;
    }

    public void setAction(ACTION_TYPE action) {
        this.action = action;
    }

    public long getPesel() {
        return pesel;
    }

    public void setPesel(int pesel) {
        this.pesel = pesel;
    }
}
