package movieDB;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by zegarski on 2017-06-29.
 */
public class Author {

    private int id;
    private Date birthday;
    private String name;
    private List<Float> marks = new LinkedList<>();

    public Author(int id, Date birthday, String name, List<Float> marks) {
        this.id = id;
        this.birthday = birthday;
        this.name = name;
        this.marks = marks;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Float> getMarks() {
        return marks;
    }

    public void setMarks(List<Float> marks) {
        this.marks = marks;
    }
}



