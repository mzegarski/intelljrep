package movieDB;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by zegarski on 2017-06-29.
 */
public class MovieDB {

    //Map<String, Author> - nazwisko na autor
    //Map<Author, List<Movie>> - autor na filmy

    List<Movie> authorMovies = new LinkedList<>();

    Map<Author, List<Movie>> movieDB = new HashMap<>();

    public void addMovie() {

    }

    public void addAuthor( ) {

    }

    public void removeMovie() {

    }

    public void removeAuthor() {

    }

    public void getAuthorList() {

    }

    public void getMovieList() {

    }

    public void searchMovie() {

    }

    public void searchAuthor() {

    }

    public void printDBToFile() {

    }

    public void getDBFromFile() {

    }


}
