package movieDB;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by zegarski on 2017-06-29.
 */
public class Movie {

    private int id;
    private float movieLength;
    private Author author;
    private String title;
    private List<Float> marks = new LinkedList<>();
    private Date release;

    public Movie(int id, float movieLength, Author author, String title, List<Float> marks, Date release) {
        this.id = id;
        this.movieLength = movieLength;
        this.author = author;
        this.title = title;
        this.marks = marks;
        this.release = release;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getMovieLength() {
        return movieLength;
    }

    public void setMovieLength(float movieLength) {
        this.movieLength = movieLength;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Float> getMarks() {
        return marks;
    }

    public void setMarks(List<Float> marks) {
        this.marks = marks;
    }

    public Date getRelease() {
        return release;
    }

    public void setRelease(Date release) {
        this.release = release;
    }
}
