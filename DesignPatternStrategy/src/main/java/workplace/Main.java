package workplace;

/**
 * Created by zegarski on 2017-06-29.
 */
public class Main {

    public static void main(String[] args) {

        Workplace work = new Workplace();

        work.addWorker(5, "Janek");
        work.addWorker(10, "Asia");
        work.addWorker(17, "Olaf");
        work.addWorker(11, "Olaf");
        work.addWorker(17, "Justyna");

        System.out.println(work.getOccupant(17));
        System.out.println(work.getOccupant(22));

        work.printAllOccupants();
        work.printAllRooms();

        System.out.println(work.findRoom("Asia"));

    }


}
