package workplace;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zegarski on 2017-06-29.
 */
public class Workplace {

    private Map<Integer, String> workplaceMap = new HashMap<>();
    private Map<String, Integer> reverseWorkplaceMap = new HashMap<>();

    public void addWorker(int roomNum, String person) {

        if (workplaceMap.containsKey(roomNum) || reverseWorkplaceMap.containsKey(roomNum))
            System.out.println("Pokoj nr " + roomNum + " jest zajety!");
        else if (workplaceMap.containsValue(person) || reverseWorkplaceMap.containsKey(person))
            System.out.println(person + " ma juz przydzielony pokoj!");
        else {
            workplaceMap.put(roomNum, person);
            reverseWorkplaceMap.put(person, roomNum);
        }
    }

    public int findRoom(String person) {

        if (!reverseWorkplaceMap.containsKey(person))
            System.out.println("Nie ma takiej osoby");

        return reverseWorkplaceMap.get(person);
    }

    public String getOccupant(int roomNum) {

        if (!workplaceMap.containsKey(roomNum))
            System.out.println("Nie ma takiego pokoju");

        return workplaceMap.get(roomNum);
    }

    public void printAllOccupants() {

        for (String p : workplaceMap.values())
            System.out.println(p);
    }

    public void printAllRooms() {

        for (int r : workplaceMap.keySet())
            System.out.println(r);
    }
}