/**
 * Created by zegarski on 2017-07-14.
 */
public enum HttpMethod {

    GET, PUT, PATCH, POST, DELETE, OPTIONS;

}
