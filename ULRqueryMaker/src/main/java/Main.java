import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by zegarski on 2017-07-14.
 */
public class Main {

    public static void main(String[] args) throws IOException {

//        exercise2();

        exercise3();




    }

    private static void exercise3() throws IOException {
        String url = "http://database.dev4.it:54000/users";

        HttpConnector hc = new HttpConnector();

        System.out.println(hc.sendGET(url));

        //System.out.println(hc.sendPOST(url, "Maciej = Zegarski"));
    }


    private static void exercise2() throws IOException {
        String url = "http://palo.ferajna.org/sda/wojciu/json.php";
        String key = "login";
        String[] valuesArray = new String[]{"admin", "adam", "operator", "test", "tester", "oper", "asd", "admin2"};

        HttpConnector hc = new HttpConnector();

        for (int i = 0; i < valuesArray.length; i++) {

            System.out.print(valuesArray[i] + ": \t");

            String params = key + "=" + valuesArray[i];
            System.out.println(hc.sendPOST(url, params));
        }
    }
}
