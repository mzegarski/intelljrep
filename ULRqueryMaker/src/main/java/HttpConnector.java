import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by zegarski on 2017-07-14.
 */
public class HttpConnector {

    private URL obj;
    private HttpURLConnection connection;
    private String userAgent = "Zegarski";


    public String sendGET(String urlInput) throws IOException {

        obj = new URL(urlInput);
        connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", userAgent);
        int responseCode = connection.getResponseCode();

        String result = "";
        if (responseCode == 200) {
            InputStream res = connection.getInputStream();
            InputStreamReader reader = new InputStreamReader(res);
            Scanner sc = new Scanner(reader);

            while (sc.hasNextLine()) {
                result += sc.nextLine();
            }
            sc.close();
        }


        return result;
    }

    public String sendPOST(String url, String params) throws IOException {

        obj = new URL(url);
        connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("User-Agent", userAgent);

        connection.setDoOutput(true);
        DataOutputStream dos = new DataOutputStream(connection.getOutputStream());
        dos.writeBytes(params);
        dos.flush();
        dos.flush();

        Scanner sc = new Scanner(new InputStreamReader(connection.getInputStream()));
        String lines = "";
        while (sc.hasNextLine())
            lines += sc.nextLine();

        sc.close();
        return lines;
    }

    public String makeRequestURL(Map<String, String> params, String url) {
        url += "?";
        for (Map.Entry<String, String> entry : params.entrySet())
            url += entry.getKey() + "=" + entry.getValue() + "&";
        return url.substring(0, url.length() - 1);
    }

}
