package linkedList;

/**
 * Created by zegarski on 2017-06-26.
 */
public class ListElement {

    private int value;

    private ListElement previousElement;

    private ListElement nextElement;

    public ListElement () {}

    public ListElement(int value, ListElement previousElement, ListElement nextElement) {
        this.value = value;
        this.previousElement = previousElement;
        this.nextElement = nextElement;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public ListElement getPreviousElement() {
        return previousElement;
    }

    public void setPreviousElement(ListElement previousElement) {
        this.previousElement = previousElement;
    }

    public ListElement getNextElement() {
        return nextElement;
    }

    public void setNextElement(ListElement nextElement) {
        this.nextElement = nextElement;
    }

}
