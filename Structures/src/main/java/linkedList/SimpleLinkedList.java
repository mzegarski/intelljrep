package linkedList;

public class SimpleLinkedList implements SimpleList {

    private int listSize = 0;
    private ListElement first;
    private ListElement last;


    public int get(int index) {

        if (index <= listSize) {
            ListElement e = first;
            for (int i = 0; i < index - 1; i++)
                e = e.getNextElement();

            return e.getValue();
        } else
            throw new IllegalArgumentException("Nie ma tylu elementow w liscie");
    }

    public void add(int value) {

        ListElement e = new ListElement();
        e.setValue(value);

        if (first == null) {
            first = e;
            last = first;

        } else {
            e.setPreviousElement(last);
            last.setNextElement(e);
            last = e;
        }
        listSize++;
    }

    public void add(int value, int index) {

        ListElement e = first;

        if (index <= listSize) {

            for (int i = 0; i < index - 1; i++)
                e = e.getNextElement();

            ListElement newElement = new ListElement(value, e.getPreviousElement(), e);

            e.getPreviousElement().setNextElement(newElement);

            e.setPreviousElement(newElement);

            listSize++;
        } else {
            throw new IllegalArgumentException("Nie ma tylu elementów na liście");
        }

    }

    public boolean contain(int value) {
        ListElement e = first;
        while (e != null) {
            if (e.getValue() == value) {
                return true;
            }
            e = e.getNextElement();
        }

        return false;
    }

    public void remove(int index) {

        ListElement e = first;

        if (index <= listSize) {


            for (int i = 0; i < index - 1; i++)
                e = e.getNextElement();

            removeFirstListElement(e);

            removeListElement(e);

            listSize--;

        } else {
            throw new IllegalArgumentException("Nie ma tylu elementów na liście");
        }

    }

    private void removeFirstListElement(ListElement e) {
        if (e == first) {
            first = first.getNextElement();
            first.setPreviousElement(null);
        }
    }

    private void removeListElement(ListElement e) {
        if (e.getPreviousElement() != null)
            e.getPreviousElement().setNextElement(e.getNextElement());

        if (e.getNextElement() != null)
            e.getNextElement().setPreviousElement(e.getPreviousElement());
    }

    public void removeValue(int value) {

        ListElement e = first;
        while (e != null) {
            if (e.getValue() == value) {

                removeFirstListElement(e);

                removeListElement(e);

                listSize--;
                break;
            }
            e = e.getNextElement();
        }

    }

    public int size() {
        return listSize;
    }


}
