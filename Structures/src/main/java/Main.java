import Queue.SimpleQueue;
import linkedList.SimpleLinkedList;
import linkedQueue.SimpleLinkedQueue;
import lists.SimpleArrayList;

/**
 * Created by zegarski on 2017-06-23.
 */
public class Main {

    public static void main(String[] args) {


        //simpleArrayList();

        //simpleLinkedList();

        //simpleQueue();

        SimpleLinkedQueue lq = new SimpleLinkedQueue();

        System.out.println(lq.isEmpty());



        for (int i = 1 ; i<=10; i++)
            lq.offer(i);

        for (int i = 1 ; i<=10; i++)
            System.out.println(lq.poll());







    }

    private static void simpleQueue() {
        SimpleQueue que = new SimpleQueue();


        que.offer(1);
        que.offer(2);
        que.offer(3);
        que.offer(4);
        que.offer(5);
        que.offer(6);
        que.offer(7);
        que.offer(8);
        que.offer(9);
        System.out.println(que.poll());
        System.out.println(que.poll());
        System.out.println(que.poll());
        System.out.println(que.poll());
        System.out.println(que.poll());
        System.out.println(que.poll());
        System.out.println(que.poll());
        System.out.println(que.poll());
        System.out.println(que.poll());
    }

    private static void simpleLinkedList() {
        SimpleLinkedList linkedList = new SimpleLinkedList();

        for (int i = 0; i < 10; i++)
            linkedList.add(i + 1);

        System.out.println("Size: " + linkedList.size());

        linkedListPrintAll(linkedList);

        System.out.println(linkedList.contain(3)); //true
        System.out.println(linkedList.contain(20)); //false

        linkedList.add(0, 5);
        linkedListPrintAll(linkedList);

        linkedList.remove(2);
        linkedListPrintAll(linkedList);


        linkedList.removeValue(5);
        linkedListPrintAll(linkedList);

        linkedList.remove(1);
        linkedListPrintAll(linkedList);

        linkedList.removeValue(3);
        linkedListPrintAll(linkedList);
    }

    private static void linkedListPrintAll(SimpleLinkedList linkedList) {
        System.out.println("Print all: ");
        for (int i = 0; i < linkedList.size(); i++) {

            System.out.println(linkedList.get(i + 1));
        }
    }

    private static void simpleArrayList() {
        SimpleArrayList list = new SimpleArrayList();
        list.add(1);
        list.add(2);
        list.add(3);

        System.out.println(list.get(0));
        System.out.println(list.get(1));
        System.out.println(list.get(2));
        System.out.println("Rozmiar: " + list.size());

        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        System.out.println(list.get(3));
        System.out.println(list.get(4));
        System.out.println(list.get(5));
        System.out.println(list.get(6));
        System.out.println("Rozmiar: " + list.size());
        System.out.println(list.contain(4));
        System.out.println(list.contain(50));

        System.out.println("Usuwanie:");
        System.out.println(list.get(2));
        System.out.println("Rozmiar: " + list.size());
        list.remove(2);
        System.out.println(list.get(2));
        System.out.println("Rozmiar: " + list.size());

        //wypisz all
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
}
