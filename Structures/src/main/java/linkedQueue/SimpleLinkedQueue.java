package linkedQueue;

import java.util.NoSuchElementException;

/**
 * Created by zegarski on 2017-06-27.
 */
public class SimpleLinkedQueue implements SimpleQueueInterface {

    private QueueElement first;
    private QueueElement last;

    public boolean isEmpty() {

        if (first == null) return true;

        return false;
    }

    public void offer(int value) {

        QueueElement e = new QueueElement();

        if (isEmpty() == true) {
            e.setValue(value);
            first = e;

        } else {
            e.setValue(value);
            last.setNextElement(e);
        }
        last = e;
    }

    public int poll() {

        int ret = peek();


            first = first.getNextElement();
            if (first == null) last = null;

        return ret;
    }

    public int peek() {

        if (!isEmpty()) return first.getValue();

        else throw new NoSuchElementException("Kolejka pusta");
    }
}
