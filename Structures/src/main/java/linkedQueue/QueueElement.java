package linkedQueue;

/**
 * Created by zegarski on 2017-06-27.
 */
public class QueueElement {

    private Integer value;

    private QueueElement nextElement;

    public QueueElement() {
    }

    public QueueElement(Integer value, QueueElement nextElement) {
        this.value = value;
        this.nextElement = nextElement;
    }

    public int getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public QueueElement getNextElement() {
        return nextElement;
    }

    public void setNextElement(QueueElement nextElement) {
        this.nextElement = nextElement;
    }


}
