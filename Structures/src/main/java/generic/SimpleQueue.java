package generic;

import java.util.NoSuchElementException;

/**
 * Created by zegarski on 2017-06-27.
 */
public class SimpleQueue<T> implements SimpleQueueInterface<T> {

    private int first;
    private int last;
    private boolean isFull = false;

    private Object[] myQueue = new Object[8];


    public boolean isEmpty() {
        if (isFull) return false;
        return (first == last);
    }

    public void offer(T value) {


        if (isFull) createBiggerArray();

        myQueue[last] = value;

        if (last + 1 >= myQueue.length) last = -1;

        last++;

        isFull = (first == last);
    }

    private void createBiggerArray() {
        Object[] newQueue = new Object[myQueue.length * 2];
        int i = 0;

        for (; i < myQueue.length; i++)
            newQueue[i] = poll();

        myQueue = newQueue.clone();
        first = 0;
        last = i;

        isFull = false;
    }

    public T poll() {

        if (isEmpty())
            throw new NoSuchElementException("Kolejka pusta");

        T ret = (T) myQueue[first];
        myQueue[first] = null;
        first++;
        return ret;
    }

    public T peek() {

        if (isEmpty())
            throw new NoSuchElementException("Kolejka pusta");
        return (T) myQueue[first];
    }
}
