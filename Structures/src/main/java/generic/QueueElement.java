package generic;

/**
 * Created by zegarski on 2017-06-27.
 */
public class QueueElement<T> {

    private T value;

    private QueueElement nextElement;

    public QueueElement() {
    }

    public QueueElement(T value, QueueElement nextElement) {
        this.value = value;
        this.nextElement = nextElement;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public QueueElement getNextElement() {
        return nextElement;
    }

    public void setNextElement(QueueElement nextElement) {
        this.nextElement = nextElement;
    }


}
