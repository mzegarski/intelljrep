package generic;

/**
 * Created by zegarski on 2017-06-27.
 */
public interface SimpleQueueInterface<T> {


    boolean isEmpty(); // sprawdza, czy kolejka jest pusta

    void offer(T value); // dodaje element do kolejki

    T poll(); // zwraca i usuwa element z kolejki

    T peek(); // zwraca element z kolejki


}
