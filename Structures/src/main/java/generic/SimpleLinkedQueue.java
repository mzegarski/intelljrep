package generic;



import java.util.NoSuchElementException;

/**
 * Created by zegarski on 2017-06-27.
 */
public class SimpleLinkedQueue<T> implements SimpleQueueInterface<T> {

    private QueueElement<T> first;
    private QueueElement<T> last;

    public boolean isEmpty() {

        if (first == null) return true;

        return false;
    }

    public void offer(T value) {

        QueueElement e = new QueueElement();
        e.setValue(value);
        if (isEmpty()) {

            first = e;

        } else {

            last.setNextElement(e);
        }
        last = e;
    }

    public T poll() {

        T ret = peek();


            first = first.getNextElement();
            if (first == null) last = null;

        return ret;
    }

    public T peek() {

        if (!isEmpty()) return first.getValue();

        else throw new NoSuchElementException("Kolejka pusta");
    }
}
