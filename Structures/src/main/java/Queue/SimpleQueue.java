package Queue;

import java.util.NoSuchElementException;

/**
 * Created by zegarski on 2017-06-27.
 */
public class SimpleQueue implements SimpleQueueInterface {

    private int first;
    private int last;
    private boolean isFull = false;

    private int[] myQueue = new int[8];


    public boolean isEmpty() {
        if (isFull == true) return false;
        return (first == last);
    }

    public void offer(int value) {


        if (isFull == true) createBiggerArray();

        myQueue[last] = value;

        if (last + 1 >= myQueue.length) last = -1;

        last++;

        isFull = (first == last);
    }

    private void createBiggerArray() {
        int[] newQueue = new int[myQueue.length * 2];
        int i = 0;

        for (; i < myQueue.length; i++)
            newQueue[i] = poll();

        myQueue = newQueue.clone();
        first = 0;
        last = i;

        isFull = false;
    }

    public int poll() {

        if (isEmpty() == true)
            throw new NoSuchElementException("Kolejka pusta");

        int ret = myQueue[first];
        first++;
        return ret;
    }

    public int peek() {

        if (isEmpty() == true)
            throw new NoSuchElementException("Kolejka pusta");
        return myQueue[first];
    }
}
