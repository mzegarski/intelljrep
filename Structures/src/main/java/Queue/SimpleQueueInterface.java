package Queue;

/**
 * Created by zegarski on 2017-06-27.
 */
public interface SimpleQueueInterface {


    boolean isEmpty(); // sprawdza, czy kolejka jest pusta

    void offer(int value); // dodaje element do kolejki

    int poll(); // zwraca i usuwa element z kolejki

    int peek(); // zwraca element z kolejki


}
