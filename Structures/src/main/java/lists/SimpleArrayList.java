package lists;

import java.util.NoSuchElementException;

/**
 * Created by zegarski on 2017-06-23.
 */
public class SimpleArrayList implements SimpleList {

    private int listSize = 0;

    private int[] myList = new int[4];

    public int get(int index) {
        if (index < listSize)
        return myList[index];
        else
            throw new NoSuchElementException();
    }

    public void add(int value) {

        if (listSize == myList.length) {
            int[] newList = new int[listSize * 2];

            for (int i = 0; i < myList.length; i++)
                newList[i] = myList[i];

            newList[listSize] = value;
            myList = newList.clone();
        } else {
            myList[listSize] = value;
        }

        listSize++;

    }

    public void add(int value, int index) {

        if (listSize == myList.length) {
            int[] newList = new int[listSize * 2];

            int i = 0;

            for (; i<index; i++){
                newList[i] = myList[i];
            }

            newList[i] = value;
            //TODO tu dokonczyc
//            for (; i<myList.length; i++){
//                newList[i] =
//            }

        }

    }

    public boolean contain(int value) {

        for (int i = 0; i < myList.length; i++)
            if (myList[i] == value)
                return true;

        return false;
    }

    public void remove(int index) {

        for (int i = index; i < myList.length - 1; i++) {
            myList[i] = myList[i + 1];
        }
    }

    public void removeValue(int value) {

    }

    public int size() {
        return myList.length;
    }
}
