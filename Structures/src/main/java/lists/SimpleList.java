package lists;

/**
 * Created by zegarski on 2017-06-23.
 */
public interface SimpleList {

    int get (int value);

    void add(int value);

    void add(int value, int index);

    boolean contain(int value);

    void remove(int index);

    void removeValue(int value);

    int size();


}
