package moviedb;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Created by zegarski on 2017-07-13.
 */
public class SimpleCRUD<T> {

    private String databasename;

    public SimpleCRUD(String databasename) {
        this.databasename = databasename;
    }

    public String getDatabasename() {
        return databasename;
    }

    public void setDatabasename(String databasename) {
        this.databasename = databasename;
    }

    public boolean insertOrUpdate(T type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.saveOrUpdate(type);
        session.save(type);
        t.commit();
        session.close();
        return true;
    }

    public void delete(T type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(type);
        t.commit();
        session.close();
    }

    public void delete(int id) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(this.get(id));
        t.commit();
        session.close();
    }

    public T get(int id) {
        T tObject = null;
        Session session = HibernateUtil.openSession();
        tObject = (T) session.load(tObject.getClass(), id);
        session.close();
        return tObject;
    }

    public List<T> get() {
        List<T> tList;
        Session session = HibernateUtil.openSession();
        tList = session.createQuery("from " + databasename).list();
        session.close();
        return tList;
    }
}
