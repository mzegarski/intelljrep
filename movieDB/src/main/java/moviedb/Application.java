package moviedb;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by zegarski on 2017-07-13.
 */
public class Application {

    private String database = "moviedb";

    private SimpleCRUD<Movie> CRUD = new SimpleCRUD(database);

    public void start() {

        welcomeMsg();
        try (Scanner scanner = new Scanner(System.in)) {

            do {
                printMenu();

                System.out.println("Twoj wybór: ");
                int choice = scanner.nextInt();

                if (choice == 1)
                    for (Movie m : CRUD.get()) System.out.println(m);

                else if (choice == 2)

                    insertOrUpdateMovie(scanner);

                else if (choice == 3) {

                } else if (choice == 4)
                    exit();


            } while (true);
        } catch (InputMismatchException e) {
            System.out.println("[ERROR] Można podać tylko cyfry.");
        }
    }

    private void insertOrUpdateMovie(Scanner scanner) {
        System.out.println("Podaj nazwę filmu: ");
        String title = scanner.nextLine();
        System.out.println("Podaj rok wydania filmu: ");
        int year = scanner.nextInt();
        System.out.println("Podaj czas trwania filmu: ");
        double duration = scanner.nextDouble();
        System.out.println("Podaj opis filmu: ");
        String description = scanner.nextLine();
        System.out.println("Podaj nazwę gatunku: ");
        String genre = scanner.nextLine();
        CRUD.insertOrUpdate(new Movie(title, year, duration, description, new Genre(genre)));
    }

    private void welcomeMsg() {
        System.out.println("Witaj w bazie danych filmów!");
        System.out.println("Pracujesz na bazie danych: " + database + "\n");
    }

    private void printMenu() {
        System.out.println("1.Pokaż rekordy");
        System.out.println("2.Dodaj/Zmodyfikuj rekord");
        System.out.println("3.Usuń rekord");
        System.out.println("4.Koniec");
    }

    private void exit() {
        System.out.println("Dowidzenia!");
        System.exit(0);
    }

}
