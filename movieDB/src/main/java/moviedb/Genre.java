package moviedb;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zegarski on 2017-07-13.
 */


@Entity
@Table(name = "Genre")
public class Genre {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id_genre;

    @Column(name = "name")
    private String name;

    @OneToMany(fetch=FetchType.LAZY,cascade = CascadeType.ALL)
    List<Movie> movieList = new ArrayList<Movie>();

    public Genre() {
    }

    public Genre(int id_genre, String name) {
        this.id_genre = id_genre;
        this.name = name;
    }

    public Genre(String name) {
        this.name = name;
    }

    public int getId_genre() {
        return id_genre;
    }

    public void setId_genre(int id) {
        this.id_genre = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Movie> getMovieList() {
        return movieList;
    }

    public void setMovieList(List<Movie> movieList) {
        this.movieList = movieList;
    }
}