package moviedb;

/**
 * Created by zegarski on 2017-07-13.
 */
public class Main {

    public static void main(String[] args) {

//        movieDBManual();

        Application app = new Application();
        app.start();

    }

    private static void movieDBManual() {
        SimpleCRUD<Movie> movieSimpleCRUD = new SimpleCRUD("moviedb");

        Genre genre_1 = new Genre("Bajka");
        Genre genre_2 = new Genre("Akcja");
        Genre genre_3 = new Genre("Porno");

        Movie mv1 = new Movie("Lion King", 1999, 120, "Bajka o lwach", genre_1);
        Movie mv2 = new Movie("Fight Club", 2001, 190, "Facet obija się po ryju i rozwala świat", genre_2);
        Movie mv3 = new Movie("Ironman", 2010, 188, "Koleś oszukuje śmierć zakładając zbroję z żelaza", genre_2);

        movieSimpleCRUD.insertOrUpdate(mv1);
        movieSimpleCRUD.insertOrUpdate(mv2);
        movieSimpleCRUD.insertOrUpdate(mv3);

//        System.out.println(movieSimpleCRUD.get(1));
    }
}
