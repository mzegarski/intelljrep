package moviedb;

import java.util.List;

/**
 * Created by zegarski on 2017-07-13.
 */
public interface AbstractDAO<T> {

    boolean insert(T type);

    boolean delete(T type);

    boolean delete(int id);

    boolean update(T type);

    T get(int id);

    List<T> get();
}


